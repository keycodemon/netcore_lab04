﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WorldJourney.Filters;
using WorldJourney.Models;

namespace WorldJourney.Controllers
{
    public class CityController : Controller
    {
        private IData _data;
        private IWebHostEnvironment _enviroment;

        public CityController(IData data, IWebHostEnvironment environment)
        {
            _data = data;
            _enviroment = environment;

            _data.CityInitializeData();
        }

        [Route("WorldJourney")]
        [ServiceFilter(typeof(LogActionFilterAttribute))]
        public IActionResult Index()
        {
            ViewData["Page"] = "Search city";
            return View();
        }

        [Route("CityDetails/{id?}")]
        public IActionResult Details(int? Id)
        {
            ViewData["Page"] = "Selected city";

            City city = _data.GetCityById(Id);
            if(city == null)
            {
                return NotFound();
            }

            ViewBag.Title = city.CityName;
            return View(city);
        }

        public IActionResult GetImage(int? CityId)
        {
            ViewData["Message"] = "Display Image";

            City requestedCity = _data.GetCityById(CityId);
            var webRootpath = _enviroment.WebRootPath;
            string folderPath = @"\images\";
            if (requestedCity != null)
            {
                var fullPath = webRootpath + folderPath + requestedCity.ImageName;
                using (var fileOnDisk = new FileStream(fullPath, FileMode.Open))
                using (var br = new BinaryReader(fileOnDisk))
                {
                    byte[] fileBytes = br.ReadBytes((int)fileOnDisk.Length);
                    return File(fileBytes, requestedCity.ImageMimeType);
                }
            }
            return NotFound();
        }
    }
}
